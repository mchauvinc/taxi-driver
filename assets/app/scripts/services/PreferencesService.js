'use strict';

angular.module('driverApp')
  .factory('PreferencesService', function () {
    var keys = {
      USERNAME: 'username',
      PASSWORD: 'password',
      NOTIFICATIONS: 'notifications',
      LOCALIZATION: 'localization',
      LANGUAGE: 'language'
    };
    return {
      Keys: keys,
      get: function(key) {
        return localStorage.getItem(key);
      },
      set: function(key, value) {
        localStorage.setItem(key, value);
      },
      remove: function(key) {
        localStorage.removeItem(key);
      },
      getUsername: function(){
        return this.get(keys.USERNAME);
      },
      getPassword: function() {
        return this.get(keys.PASSWORD);
      }
    };
  });
