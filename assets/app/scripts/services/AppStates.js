'use strict';

angular.module('driverApp')
  .factory('AppStates', function () {
    var states = {
      'AppStates:LANGUAGE': 'english'
    };
    var keys = {
      CURRENT_POSITION: 'AppStates:CURRENT_POSITION',
      COUNTRY_PROMISE: 'AppStates:COUNTRY_PROMISE',
      LANGUAGE: 'AppStates:LANGUAGE',
      TOKEN: 'AppStates:TOKEN',
      EMAIL: 'AppStates:EMAIL',
      TRIP: 'AppStates:TRIP',
      RECEIVED_TRIPS:'AppStates:RECEIVED_TRIP'
    }
    return {
      Keys: keys,
      set: function(key, state) {
        // console.log(key, state);
        states[key] = state;
      },
      get: function(key) {
        // console.log(key, states[key]);
        return states[key];
      }
    }
  });
