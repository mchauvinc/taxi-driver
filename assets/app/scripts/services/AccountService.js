'use strict';

angular.module('driverApp')
  .provider('AccountService', function () {
    // Service logic
    // ...

    var urls = {
      api: 'http://taxi-nova-web.azurewebsites.net/api/values/',
      login: 'LoginDriver',
      logout: 'LogoutDriver',
      forgot_password: 'ForgotPasswordDriver',
      current_job: 'GetCurrentJob',
      past_jobs:'GetPastJobs',
      registerDriver:'SignUpDriver',
      driver_information:'DriverProfile',
      change_password:'ChangePassword',
      response:'DriverResponse'
    }

    this.setUrl = function(url){
      urls.api = url;
    }

    function getUrl(k) {
      return urls.api + urls[k];
    }

    this.$get = ['$rootScope', '$http', function($rootScope, $http){
      var localEvents = {
        LOAD_STARTED: 'AccountService:LOAD_STARTED',
        LOAD_STOPPED: 'AccountService:LOAD_STOPPED',
        LOGIN_SUCCEEDED: 'AccountService:LOGIN_SUCCEEDED',
        LOGIN_FAILED: 'AccountService:LOGIN_FAILED',
        LOGIN_ERROR: 'AccountService:LOGIN_ERROR',
        LOGOUT_SUCCEEDED: 'AccountService:LOGOUT_SUCCEEDED',
        LOGOUT_ERROR: 'AccountService:LOGOUT_ERROR',
        PASSWORD_RESET_SUCCEEDED: 'AccountService:PASSWORD_RESET_SUCCEEDED', //succeeded means the API call has succeeded
        PASSWORD_RESET_ERROR: 'AccountService:PASSWORD_RESET_ERROR',
        CURRENT_JOB_LOADED:'AccountService:CURRENT_JOB_LOADED',
        PAST_JOBS_LOADED: 'AccountService:PAST_JOBS_LOADED',
        REGISTER_SUCCEEDED: 'AccountService:REGISTER_SUCCEEDED',
        REGISTER_FAILED: 'AccountService:REGISTER_FAILED',
        INFORMATION_LOADED:'AccountService:INFORMATION_LOADED',
        INFORMATION_FAILED:'AccountService:INFORMATION_FAILED',
        NEW_PASSWORD_SUCCESS:'AccountService:NEW_PASSWORD_SUCCESS',
        NEW_PASSWORD_FAILED:'AccountService:NEW_PASSWORD_FAILED',
        DECLINE_OK:'AccountService:DECLINE_OK',
        DECLINE_FAIL:'AccountService:DECLINE_FAIL',
        ACCEPT_OK:'AccountService:ACCEPT_OK',
        ACCEPT_FAIL:'AccountService:ACCEPT_FAIL'
      }

      function startLoad() {
        $rootScope.$broadcast(localEvents.LOAD_STARTED);
      }
      function stopLoad() {
        $rootScope.$broadcast(localEvents.LOAD_STOPPED);
      }
      // Public API here
      return {
        Events: localEvents,
        login: function (username, password) {
          startLoad();
          $http.post(getUrl('login'),{
            email: username,
            password: password
          }).success(function(token){
            console.log(token, 'in AccountService');
            if(token && token != 'null'){
              $rootScope.$broadcast(localEvents.LOGIN_SUCCEEDED, token);
            }else {
              // DEBUG
              $rootScope.$broadcast(localEvents.LOGIN_FAILED);
              // $rootScope.$broadcast(localEvents.LOGIN_FAILED);
            }
            stopLoad();
          }).error(function(){
            // DEBUG
              $rootScope.$broadcast(localEvents.LOGIN_FAILED);
            // $rootScope.$broadcast(localEvents.LOGIN_ERROR);
            stopLoad();
          });
        },
        
        logout: function(token){
          startLoad();
          $http.post(getUrl('logout'),{
            token: token
          }).success(function(){
            $rootScope.$broadcast(localEvents.LOGOUT_SUCCEEDED);
            stopLoad();
          }).error(function(){
            $rootScope.$broadcast(localEvents.LOGOUT_ERROR);
            stopLoad();
          });
        },

          registerDriver: function(driver_FName, driver_LName, driver_PhoneNumber, driver_email, driver_password,carModel) {
          startLoad();
          contentType: "application/json;charset=utf-8",
           $http.post(getUrl('registerDriver'),{
            Email:driver_email,
            password:driver_password,
            FName:driver_FName,
            LName:driver_LName,
            PhoneNumber:driver_PhoneNumber,
            carModel:carModel
           }).success(function(token){
            if(token){
              $rootScope.$broadcast(localEvents.REGISTER_SUCCEEDED, token);
              console.log('register ok');
              stopLoad();
            }
           }).error(function(){
            $rootScope.$broadcast(localEvents.REGISTER_FAILED);
            //The driver already exist in DataBase or password is not accepted
            stopLoad();
           });
        },

        forgotPassword: function(user_email){ //TO DO : Check that API call
          startLoad();
          $http.post(getUrl('forgot_password'),{
            email: user_email
          }).success(function(details){
              $rootScope.$broadcast(localEvents.PASSWORD_RESET_SUCCEEDED, details);
              console.log(localEvents.PASSWORD_RESET_SUCCEEDED);
            stopLoad();
          }).error(function(data){
            $rootScope.$broadcast(localEvents.PASSWORD_RESET_ERROR);
              console.log(localEvents.PASSWORD_RESET_ERROR);
            stopLoad();
          });
        },

        getCurrentJob: function(token){
          startLoad();
          $http.post(getUrl('current_job'),{
            Session:token
          }).success(function(current){
              $rootScope.$broadcast(localEvents.CURRENT_JOB_LOADED, current);
              console.log('getCurrentJob ok');
              stopLoad();
           }).error(function(){
            var res=[{
            pickup: 'pickup 3',
            destination:'destination 3',
            time:'4.45am',
            duration: '20 mins',
            passengers: '2',
            length: '2 km',
            date:'3rd June 2012',
            location:'location 3',
            driver:{
              name:'Darth Vador',
              company:'Dark side taxi',
              plate:'6778-AC',
              contact:'number5'
            }
          }];
          $rootScope.$broadcast(localEvents.CURRENT_JOB_LOADED, res);
          stopLoad();
          })
        },

        getPastJobs: function(token){
          startLoad();
          $http.post(getUrl('past_jobs'),{
            token:token
          }).success(function(res){
              $rootScope.$broadcast(localEvents.PAST_JOBS_LOADED, res);
              stopLoad();
          }).error(function(){
          var res=[{
            pickup: 'pickup 3',
            destination:'destination 3',
            time: '4.45am',
            duration: '20 mins',
            passengers: '2',
            length: '2 km',
            date:'3rd June 2012',
            location:'location 3',
            driver:{
              name:'Wolverine',
              company:'X-men',
              plate:'97218-AC',
              contact:'number3'
            }
          },
          {
          pickup: 'pickup 4',
          destination:'destination 4',
            time: '8.54pm',
            duration: '24 mins',
            passengers: '3',
            length: '14 km',
            date:'8th may 2013',
            location:'location 4',
            driver:{
              name:'Jackie Choune',
              company:'Loco loca',
              plate:'89873-54',
              contact:'number4',
            }
          }
          ];
        $rootScope.$broadcast(localEvents.PAST_JOBS_LOADED, res);
        stopLoad();
          })
        },

        getDriverInformation:function(token){
          console.log(token);
          $http.post(getUrl('driver_information'),{
            Session: token
          }).success(function(res){

            $rootScope.$broadcast(localEvents.INFORMATION_LOADED,res);
            console.log('getDriverInformation:ok')
          }).error(function(){
            var res={
              name:'Fail',
              company:'',
              id:'',
              plate:'',
              contact:'',
              car:'',
              seats: ''
            };
          $rootScope.$broadcast(localEvents.INFORMATION_LOADED,res);
          console.log('getDriverInformation:return an error')
            // $rootScope.$broadcast(localEvents.INFORMATION_FAILED);
          });
        },

        changePassword:function(email,token,password){
          $http.post(getUrl('change_password'),{
            Email:email,
            Session: token,
            Password: password,
            type:'Driver'
          }).success(function(res){

            $rootScope.$broadcast(localEvents.NEW_PASSWORD_SUCCESS,res);

          }).error(function(){
            
             $rootScope.$broadcast(localEvents.NEW_PASSWORD_FAILED);
          });
        },

        DeclineTrip:function(token,tripId){
          $http.post(getUrl('response'),{
            token: token,
            tripId: tripId,
            response:0
          }).success(function(){

            $rootScope.$broadcast(localEvents.DECLINE_OK);

          }).error(function(){
            
             $rootScope.$broadcast(localEvents.DECLINE_FAIL);
          });
        },

        AcceptTrip:function(token,tripId){
          $http.post(getUrl('response'),{
            token: token,
            tripId: tripId,
            response:0
          }).success(function(){

            $rootScope.$broadcast(localEvents.ACCEPT_OK);

          }).error(function(){
            
             $rootScope.$broadcast(localEvents.ACCEPT_FAIL);
          });

        }

      };
    }];
  });
