'use strict';

angular.module('driverApp')
  .factory('TaxiDriverSocketService', ['SocketService', function (SocketService) {
    var localEvents = {
      REGISTER: 'taxi:register',
      UNREGISTER: 'taxi:unregister',
      REGISTERED: 'taxi:registered',
      UNREGISTERED: 'taxi:unregistered',
      LOCATION: 'taxi:position:update',
      TRIP_CREATED:'trip:created'
    }
    // Public API here
    return {
      Events: localEvents,
      register: function(token){
        SocketService.emit(localEvents.REGISTER, token);
      },
      unregister: function(token){
        SocketService.emit(localEvents.UNREGISTER, token);
      },
      updateLocation: function(token, latitude, longitude){
        var coord={
          lat:latitude,
          lng:longitude
        }
        SocketService.emit(localEvents.LOCATION, token,coord);
      }
    };
  }]);
