'use strict';

angular.module('socketIo',[])
  .provider('SocketService', function () {
    var apiUrl = "",              
      localEvents = {
        SOCKET_CONNECTED: "SocketService:SOCKET_CONNECTED",
        SOCKET_DISCONNECTED: "SocketService:SOCKET_DISCONNECTED",
        CONNECTED: 'connect'
    };
                  
    // Config methods
    this.setSocketUrl = function(s) {
          apiUrl = s;
          return true;
    };
    var callbacks = [];
    window.socket = {
      on: function(eventName, callback) {
        callbacks.push({eventName: eventName, callback: callback}); 
      }
    };
    this.$get = ['$rootScope', function($rootScope) {
      // socket = io.connect(apiUrl);
      return {
        Events: localEvents,
        connect: function() {
          socket = io.connect(apiUrl);
          // Add all callbacks that had been queued
          _.each(callbacks, function(c){
            socket.on(c.eventName, c.callback);
          });
          callbacks = [];
        },
        disconnect: function(){
          socket.disconnect();
        },
        on: function (eventName, callback) {
          socket.on(eventName, function () {  
            var args = arguments;
            $rootScope.$apply(function () {
              callback.apply(socket, args);
            });
          });
        },
        emit: function (eventName, data, callback) {
          socket.emit(eventName, data, function () {
            var args = arguments;
            $rootScope.$apply(function () {
              if (callback) {
                callback.apply(socket, args);
              }
            });
          });
        }
      };
    }];
  });