// Define state
angular.module('driverApp', ['hmTouchEvents', 'ui', 'ui.state', 'taxinowGoogleMaps', 'socketIo'])
  .config(function ($stateProvider, $routeProvider, $locationProvider ) {
    $routeProvider.when('/', {
      templateUrl: 'views/startup.html',
      controller: 'StartupCtrl'
    })
    .when('/connection', {
      templateUrl: 'views/connection.html',
      controller: 'ConnectionCtrl'
    })
    .when('/login', {
      templateUrl: 'views/login.html',
      controller: 'LoginCtrl'
    })
    .when('/forgotpassword', {
      templateUrl: 'views/forgotpassword.html',
      controller: 'ForgotpasswordCtrl'
    })
    .when('/tripreceive', {
      templateUrl: 'views/tripreceive.html',
      controller: 'TripreceiveCtrl'
    })
    .when('/myjobs', {
      templateUrl: 'views/myjobs.html',
      controller: 'MyjobsCtrl'
    })
    .when('/signup', {
      templateUrl: 'views/signup.html',
      controller: 'SignupCtrl'
    })
    .when('/myprofile', {
      templateUrl: 'views/myprofile.html',
      controller: 'MyprofileCtrl'
    })
    .when('/tripdetails', {
      templateUrl: 'views/tripdetails.html',
      controller: 'TripdetailsCtrl'
    })
    .when('/map', {
      templateUrl: 'views/map.html',
      controller: 'MapCtrl'
    })
    .otherwise({
        redirectTo: '/'
    });
  }).config(function(SocketServiceProvider){
    SocketServiceProvider.setSocketUrl('http://localhost:3000');
  });
