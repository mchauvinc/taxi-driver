'use strict';

angular.module('driverApp')
  .controller('LanguageCtrl', ['$scope', 'PreferencesService', function ($scope, PreferencesService) {
    $scope.saveLanguage = function(which) {
    	PreferencesService.set(PreferencesService.Keys.LANGUAGE, which);
    	$scope.goTo('tutorial');
    }
  }]);
