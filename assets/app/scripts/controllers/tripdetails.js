'use strict';

angular.module('driverApp')
  .controller('TripdetailsCtrl', [ '$scope', 'AppStates', function ($scope, AppStates) {
    
    $scope.trip = null;

    var trip = AppStates.get(AppStates.Keys.TRIP);

    if(trip)
    {
        $scope.trip =  trip;
    }


    $scope.backTap = function()
    {
        $scope.goTo('myBookings');
    }

    $scope.currentTrip = function()
    {
        $scope.goTo('activebooking');
    }
  }]);
