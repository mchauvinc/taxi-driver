'use strict';

angular.module('driverApp')
  .controller('SignupCtrl',  ['$scope','AccountService',function ($scope, AccountService) {
    $scope.step = 1;
	$scope.FName = '';
	$scope.LName='';
	$scope.PhoneNumber='';
	$scope.email='';
	$scope.password='';
	$scope.confirm_password='';
	$scope.result_signUp='';
	$scope.carModel='';

 var localEvents={
 	MISSING_INFORMATION:'driverSignUp:MISSING_INFORMATION',
 	COMPLETE_INFORMATION:'driverSignUp:COMPLETE_INFORMATION',
 	PASSWORD_MATCH:'driverSignUp:PASSWORD_MATCH',
 	PASSWORD_NOT_MATCH:'driverSignUp:PASSWORD_NOT_MATCH'
 }
 	$scope.$on(AccountService.Events.REGISTER_SUCCEEDED, function(e,token){

 		$scope.result_signUp=AccountService.Events.REGISTER_SUCCEEDED;
 		console.log(AccountService.Events.REGISTER_SUCCEEDED);
 		$scope.goTo('login');
 	});

 	$scope.$on(AccountService.Events.REGISTER_FAILED, function(e){
 		$scope.result_signUp=AccountService.Events.REGISTER_FAILED;
 		console.log(AccountService.Events.REGISTER_FAILED);
 		$scope.goTo('login');
 	})

	$scope.next=function(email,password, confirm_password){
		$scope.email =email;
		$scope.password = password;
		$scope.confirm_password = confirm_password;

		//Check if one of attributes is empty
		if (!$scope.email || !$scope.password || !$scope.confirm_password){
			alert(localEvents.MISSING_INFORMATION);
		}
		//if password and confirm_password are the same we send information to the API
		if($scope.compare_passwords()){
			//API call
			AccountService.registerDriver($scope.FName, $scope.LName, $scope.PhoneNumber, $scope.email, 
				$scope.password,$scope.carModel);
		
		}
		
	}

	$scope.next_step=function(driver_FName, driver_LName, driver_PhoneNumber,driver_carModel){
		$scope.FName = driver_FName;
		$scope.LName = driver_LName;
		$scope.PhoneNumber = driver_PhoneNumber;
		$scope.carModel= driver_carModel;
		//Check if one of attributes is empty
		if (!$scope.FName || !$scope.LName || !$scope.PhoneNumber || !$scope.carModel){
			alert(localEvents.MISSING_INFORMATION);
		}
		else{
			//display the second part with email and passwords
			$scope.step=2;
		}	
	}

    $scope.compare_passwords = function(){
    	var res = 0;
   		if($scope.confirm_password==$scope.password){
   			//$scope.$broadcast(localEvents.PASSWORD_MATCH,password);
   			res = 1;
   			console.log(localEvents.PASSWORD_MATCH);
   		}
   		else{
    		//$scope.$broadcast(localEvents.PASSWORD_NOT_MATCH);
    		console.log(localEvents.PASSWORD_NOT_MATCH);
    		alert(localEvents.PASSWORD_NOT_MATCH);
    	}
    	return res;
    }

    $scope.back = function(){
    	if($scope.step==2){
    		$scope.step=1;
    	}
    	else{
    		$scope.goTo('login');
    	}
    }
  }]);