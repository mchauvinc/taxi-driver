'use strict';

angular.module('driverApp')
  .controller('LoginCtrl', ['$scope', 'AccountService', 'PreferencesService', '$rootScope', 'AppStates', 
    function ($scope, AccountService, PreferencesService, $rootScope, AppStates) {

  	$scope.message = "";

  	$scope.$on(AccountService.Events.LOGIN_SUCCEEDED, function(e, token){
  		PreferencesService.set(PreferencesService.Keys.USERNAME, $scope.username);
  		PreferencesService.set(PreferencesService.Keys.PASSWORD, $scope.password);
      AppStates.set(AppStates.Keys.EMAIL,$scope.username);
  		$scope.goTo('map');
  	});

  	$scope.$on(AccountService.Events.LOGIN_FAILED, function(){
  		$scope.message = "Login and password do not match";
  	});

  	$scope.$on(AccountService.Events.LOGIN_ERROR, function(){
  		$scope.message = "Unable to connect to server";
  	});

    $scope.login = function() {
      console.log('login ok')
    	AccountService.login($scope.username, $scope.password);
    }
    $scope.forgotpassword=function(){
      $scope.goTo('forgotpassword')
    }
    $scope.signUp=function(){
      $scope.goTo('signup')
    }
  }]);
