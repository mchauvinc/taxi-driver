'use strict';

angular.module('driverApp')
  .controller('MyjobsCtrl',[ '$scope','$rootScope','AccountService', 'AppStates',
    function ($scope, $rootScope,AccountService,AppStates) {
 
  	$scope.selection='past';
  	$scope.current=[];
  	$scope.pastJobs=[];
  	
  	var localEvents ={

  		SELECTED_BOOKING : "MyBookingsCtrl:SELECTED_BOOKING"
  	}

  	$scope.past=function(){

  		$scope.selection='past'
  	
  	}

  	$scope.inAdvance=function(){
  		$scope.selection='inAdvance'
  		
  	}

  	$scope.viewMore=function(){
  		//$rootScope.$broadcast(localEvents.SELECTED_BOOKING,this.booking),
      AppStates.set(AppStates.Keys.SELECTED_BOOKING,this.job),
      console.log(this.job),
  		$scope.goTo('tripdetails')
  	}


    $scope.$on(AccountService.Events.CURRENT_JOB_LOADED,function(e,job){
    $scope.current=job;
    });

    $scope.$on(AccountService.Events.PAST_JOBS_LOADED,function(e,job){
    $scope.pastBookings=job;
    });

    var token=AppStates.get(AppStates.Keys.TOKEN);

    AccountService.getCurrentJob(token);
    AccountService.getPastJobs(token);

  }]);
    
