'use strict';

angular.module('driverApp')
  .controller('TripreceiveCtrl',['$scope','AppStates','AccountService', 
    function ($scope,AppStates,AccountService) {
    $scope.current=null;
    $scope.trips=[];

    $scope.trips=AppStates.get(AppStates.Keys.RECEIVED_TRIPS);
    
    if($scope.trips!=[]){

    $scope.current=$scope.trips[0];
    }



    $scope.$on(AccountService.Events.DECLINE_OK,function(e,u){

    	$scope.trips.shift();
    	if($scope.trips!=[]){

    	$scope.current=$scope.trips[0];
    	}
    	else{
    		//maybe a goTo
    	}

    })

    $scope.$on(AccountService.Events.ACCEPT_OK,function(e,trip){

    	// $scope.goTo('')
    	// DO THINGS
    })

    $scope.response=function(dir){

    	if (dir) {

    		AccountService.AcceptTrip(token,tripId,1);

    	}
    	else if(!dir){
    		AccountService.DeclineTrip(token,tripId,0);

    	}

    }

  }]);
