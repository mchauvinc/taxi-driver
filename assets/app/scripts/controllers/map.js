'use strict';

angular.module('driverApp')
  .controller('MapCtrl', ['$scope', 
    'GeolocationService', 
    'tgmCommunicationService', 
    '$q', 
    'AppStates', 
    'PreferencesService', 
    'AccountService',
    function ($scope, GeolocationService, tgmCommunicationService, $q, AppStates, PreferencesService, AccountService) {
  	$scope.newJob=false;
    var map,
  		autocompleteService = new google.maps.places.AutocompleteService(),
  		mapDeferred = $q.defer(),
      markerDeferred= $q.defer(),
      positionDeferred=$q.defer(),
  		country = AppStates.get(AppStates.Keys.COUNTRY_PROMISE);
  	$scope.coordinates = {
  		latitude: 0,
  		longitude: 0
  	};
  	$scope.results = [];

    // Ensure that we have a position either from AppStates or from GeoLocationService
    var currentPosition=AppStates.get(AppStates.Keys.CURRENT_POSITION);
    if(currentPosition){
      positionDeferred.resolve(currentPosition);
    } else{
      var pFunc = function (e, position) {
        positionDeferred.resolve(position);
      };
      $scope.$on(GeolocationService.Events.POSITIONED, pFunc);
    }
    

    AppStates.set(AppStates.Keys.LANGUAGE, PreferencesService.get(PreferencesService.Keys.LANGUAGE));
    // var map = tgmCommunicationService.get(tgmCommunicationService.Types.MAP, "");
    $scope.$on(tgmCommunicationService.Events.ITEM_ADDED, function(e,type, name, obj){
    	if(type == tgmCommunicationService.Types.MAP){
    		map = obj;
			 mapDeferred.resolve(obj);
		}
    else if(type==tgmCommunicationService.Types.MARKER && name=='taxiMarker'){
      markerDeferred.resolve(obj);
    }
    });
    // This will ensure that the map is created
    $q.all([mapDeferred.promise, markerDeferred.promise, positionDeferred.promise]).then(function(res){
      var marker=res[1];
      var position=res[2];
      console.log(marker,position);
      marker.setMap(map);
      marker.setPosition(position);
      map.setCenter(position);
      $scope.$on(GeolocationService.Events.POSITIONED, function(e,position){
        console.log(position);
        map.setCenter(position);
        marker.setPosition(position);
      });
    });

    // NEW TRIPS ARRIVED
    $scope.$on('SOMEEVENTS',function(e,trips){

      $scope.trips=trips;
      $scope.newJob=true;
      if($scope.trips!=[]){
         $scope.trip=$scope.trips.shift();
      }

    });

    $scope.$on(AccountService.Events.DECLINE_OK,function(){

      if($scope.trips!=[]){
      $scope.trip=$scope.trips.shift();
      }
      else{
        $scope.newJob=false;
      }

    });

    $scope.$on(AccountService.Events.ACCEPT_OK,function(e,trip){

      $scope.newJob=false;
      // DO THINGS
    });

    $scope.response=function(dir){

      if (dir) {

        AccountService.AcceptTrip(token,tripId,1);

      }
      else if(!dir){
        AccountService.DeclineTrip(token,tripId,0);

      }

    }


  }]);
