'use strict';

angular.module('driverApp')
  .controller('MyprofileCtrl', ['$scope','AppStates','AccountService',
   function ($scope,AppStates,AccountService) {
    $scope.driver=null;
    $scope.password='';
    $scope.confirm_password='';

	var token=AppStates.get(AppStates.Keys.TOKEN);
    var email=AppStates.get(AppStates.Keys.EMAIL);

    $scope.changePassword=function(){

        if($scope.compare_passwords()){
    	AccountService.changePassword(email, token, $scope.password)
        }
    }

    $scope.compare_passwords = function(){
        var res = 0;
        if($scope.confirm_password==$scope.password){
            //$scope.$broadcast(localEvents.PASSWORD_MATCH,password);
            res = 1;
            console.log('PASSWORD_MATCH')
        }
        else{
            //$scope.$broadcast(localEvents.PASSWORD_NOT_MATCH);
            console.log('PASSWORD_NOT_MATCH')
        }
        return res;
    }

    $scope.$on(AccountService.Events.INFORMATION_LOADED,function(e,res){

    	$scope.driver=res;
    	console.log($scope.driver)
    }),

    $scope.$on(AccountService.Events.NEW_PASSWORD_SUCCESS,function(e,res){

        if(res=='true'){
            alert('Your password has been changed')
        }
        else{
            alert('Your password did not change')

        }
    	//DO SOMETHING LIKE ALERT
    }),
    AccountService.getDriverInformation(token);

  }]);
