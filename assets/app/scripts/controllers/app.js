'use strict';

angular.module('driverApp')
  .controller('AppCtrl', ['$rootScope',
        '$location',
        'AppStates',
        '$q',
        'GeolocationService',
        'AccountService',
        'SocketService',
        'TaxiDriverSocketService',
    function ($rootScope, $location, AppStates, $q, GeolocationService, AccountService, SocketService, TaxiDriverSocketService) {
        $rootScope.goTo = function(where){
        	$location.path('/'+where);
        };
        $rootScope.$on(AccountService.Events.LOGOUT_SUCCEEDED, function(){
            var token = AppStates.get(AppStates.Keys.TOKEN);
            AppStates.set(AppStates.Keys.TOKEN, '');
            TaxiDriverSocketService.unregister(token);
        });

        var fn = function(){
            var token = AppStates.get(AppStates.Keys.TOKEN);
            console.log(token);
            if(token) {
                TaxiDriverSocketService.register(token);
            }
        };

        SocketService.on(SocketService.Events.CONNECTED, fn);
        SocketService.connect();

        var updateLocation = function() {
            GeolocationService.getCurrentPosition(function(position){
                var token=AppStates.get(AppStates.Keys.TOKEN);
                TaxiDriverSocketService.updateLocation(token,position.coords.latitude, position.coords.longitude);
                position = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                // TaxiDriverSocketService.updateLocation(AppStates.get(AppStates.Keys.TOKEN),3.111281, 101.629457);
                // position=new google.maps.LatLng(3.111281, 101.629457);
                $rootScope.$broadcast(GeolocationService.Events.POSITIONED, position);
                AppStates.set(AppStates.Keys.CURRENT_POSITION, position);
            }, function(){}, function(){});
        }

        // SocketService.on(TaxiDriverSocketService.Events.REGISTERED, function(){
            AppStates.set( AppStates.Keys.CONNECTED, true );
            AppStates.set( AppStates.Keys.LOCATION_INTERVAL, setInterval(updateLocation, 10000));

            $rootScope.$broadcast(TaxiDriverSocketService.Events.REGISTERED);
         // });
        SocketService.on(TaxiDriverSocketService.Events.UNREGISTERED, function(){
            AppStates.set( AppStates.Keys.CONNECTED, false );
            clearInterval(AppStates.get(AppStates.Keys.LOCATION_INTERVAL));
            $rootScope.$broadcast(TaxiDriverSocketService.Events.UNREGISTERED);

        });

        SocketService.on(TaxiDriverSocketService.Events.TRIPS_CREATED, function(e,trips){

            console.log('Trip reçu',trips);

        });

        $rootScope.$on(AccountService.Events.LOGIN_SUCCEEDED, function(e, token){
            AppStates.set(AppStates.Keys.TOKEN, token);
            TaxiDriverSocketService.register(token);
        });

        
    }
  ]);
