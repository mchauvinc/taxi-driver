'use strict';

angular.module('driverApp')
  .controller('ForgotpasswordCtrl', ['$scope','AccountService', function ($scope,AccountService) {
    $scope.email ="";
    $scope.result="";

    var localEvents={
 	MISSING_INFORMATION:'ForgotPassword:MISSING_INFORMATION',
 	COMPLETE_INFORMATION:'ForgotPassword:COMPLETE_INFORMATION',
 	PASSWORD_RESET_DRIVER_SUCCEEDED: 'AccountService:PASSWORD_RESET_DRIVER_SUCCEEDED',
    PASSWORD_RESET_DRIVER_ERROR: 'AccountService:PASSWORD_RESET_DRIVER_ERROR'
 	}

 	//listen all events informing user if password had been reset
 	$scope.$on(AccountService.Events.PASSWORD_RESET_SUCCEEDED,function(e,details){
 		//details will explain the reason : 
 		//"check your email" means reset successfully or "user doesn't exist" means no need to reset
    	console.log(details);
    	$scope.result = details;
	});


 	$scope.send=function(email){
 		$scope.email = email
 		console.log($scope.email);
 		//Check if the email is empty
		if (!$scope.email){
			alert(localEvents.MISSING_INFORMATION);
		}
		else{
			//We send the user email to the API and it will reset it if needed
			AccountService.forgotPassword($scope.email);
		}
 	}

 	$scope.back=function(){
 		$scope.goTo('login');
 	}

  }]);
