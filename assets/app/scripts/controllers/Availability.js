'use strict';

angular.module('driverApp')
  .controller('AvailabilityCtrl', ['$scope', 'AppStates', 'TaxiDriverSocketService', 
    function ($scope, AppStates, TaxiDriverSocketService) {
        
    $scope.disconnected = !AppStates.get( AppStates.Keys.CONNECTED );

    $scope.$on(TaxiDriverSocketService.Events.REGISTERED, function(){
    	$scope.disconnected = !AppStates.get( AppStates.Keys.CONNECTED );
    });
    $scope.$on(TaxiDriverSocketService.Events.UNREGISTERED, function(){
    	$scope.disconnected = !AppStates.get( AppStates.Keys.CONNECTED );
    });

    $scope.connect = function(dir) {
    	var token = AppStates.get(AppStates.Keys.TOKEN);
        if(dir && $scope.disconnected){
            TaxiDriverSocketService.register(token);
        }
        else if(!dir && !$scope.disconnected){
    		TaxiDriverSocketService.unregister(token);
    	}
    }
  }]);