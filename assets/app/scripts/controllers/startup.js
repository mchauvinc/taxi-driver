'use strict';

angular.module('driverApp')
  .controller('StartupCtrl', ['$scope', 'PreferencesService', 'AccountService', '$rootScope', function ($scope, PreferencesService, AccountService, $rootScope ) {
    var username = PreferencesService.getUsername();
    var password = PreferencesService.getPassword();
    $scope.$on(AccountService.Events.LOGIN_SUCCEEDED, function(){
    	$rootScope.goTo('connection')
    });
    $scope.$on(AccountService.Events.LOGIN_FAILED, function(){
    	$rootScope.goTo('login');
    });
    $scope.$on(AccountService.Events.LOGIN_ERROR, function(){
    	$rootScope.goTo('login');
    });
    if(!username || !password){
    	$rootScope.goTo('login');
    } else {
    	AccountService.login(username, password);
    }
  }]);
