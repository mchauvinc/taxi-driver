'use strict';

angular.module('driverApp')
  .controller('MenuCtrl',['$scope','AccountService','AppStates', function ($scope,AccountService,AppStates) {
	$scope.open = false;
    $scope.shown=true;
    $scope.toggleMenu = _.throttle(function() {
    	$scope.open = !$scope.open;
    },500, true );


    $scope.logout=function(){

        AccountService.logout(AppStates.get(AppStates.Keys.TOKEN));
    }

    // Things that should always happen
    var fn = function(){
    	$scope.open = false;
    }
    $scope.$on('$stateChangeStart' , fn);

    $scope.$on('$destroy', function() {
    	$scope.$off(fn);
    });

    $scope.$on('$stateChangeSuccess' , function(e, state){
    	$scope.shown = !state.hideMenu;
    });
  }]);
