'use strict';

describe('Controller: AvailabilityCtrl', function () {

  // load the controller's module
  beforeEach(module('clientApp'));

  var AvailabilityCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AvailabilityCtrl = $controller('AvailabilityCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
